# Set of scripts for sboathud telemetry files

`delta_create.py`: create a delta file for sboathud  
`line_comparison.py`: generate a image/video of the boats' path, optionally takes a image with coordinates as background (can be generated with tools like unmined).  
`telemetry_graph.py`: plot the data from telemetry files onto a graph, use distance along track to align x axis when given a delta file.  
