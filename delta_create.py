import pandas as pd
import argparse

def to_tick(x):
    return int(x * 20)

def to_sec(x):
    return x / 20

def clamp(x, lower, upper):
    return max(lower, min(x, upper))

minecraft_dir = "C:/mc_1.20"

parser = argparse.ArgumentParser()
parser.add_argument("file")
parser.add_argument("-s", "--start_line", default=2, type=int, help="line number of telemetry file at the starting point (default=2)")
parser.add_argument("-d", "--duration", default=20070831, type=float, help="duration of the lap (default=inf)")
parser.add_argument("-i", "--interval", default=0.05, type=float, help="time interval for checkpoints (default=0.05)")
parser.add_argument("-e", "--end", action="store_true", help="add an impossible checkpoint at the end (used on point to point tracks to prevent starting a new lap)")
parser.add_argument("-o", "--output", nargs="?", default=None, help="save the file with the filename given")
parser.add_argument("-c", "--copy", action="store_true", help="copy the trimmed telemetry file to the same filename")
args = parser.parse_args()

df = pd.read_csv(args.file)
length = len(df)

start_row = args.start_line - 2
start_row = clamp(start_row, 0, length - 2)
end_row = start_row + to_tick(args.duration)
end_row = clamp(end_row, start_row, length - 1)
interval = to_tick(args.interval)
interval = clamp(interval, 1, length - 1)
rows = [i for i in range(start_row, end_row, interval)]

df["xDir"] = round((df["x"].shift(-1) - df["x"]) / df["speed"].shift(-1) * 20, 6)
df["zDir"] = round((df["z"].shift(-1) - df["z"]) / df["speed"].shift(-1) * 20, 6)
df["dist"] = df["x"] * df["xDir"] + df["z"] * df["zDir"]

df = df.iloc[rows]
df["time"] = ["{:.2f}".format(i * 0.05) for i in range(len(df))]

output = args.output if args.output else "_" + args.file.split("/")[-1].split("\\")[-1]
df[["time", "speed", "xDir", "zDir", "dist"]].to_csv(minecraft_dir + "/config/sboathud/deltas/" + output, index=False, float_format="{:.6f}".format)
if args.end:
    with open(minecraft_dir + "/config/sboathud/deltas/" + output, "a") as f:
        f.write("0,0,0,0,1\n")
if args.copy:
    df[["speed", "slipAngle", "x", "z", "y", "directionFacing", "steering", "throttle"]].to_csv(minecraft_dir + "/config/sboathud/telemetry/" + output, index=False, float_format="{:.6f}".format)
