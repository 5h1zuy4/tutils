import sys
import argparse
import matplotlib.pyplot as plt
from matplotlib import animation
import pandas as pd

colours = ["b", "r", "g", "c", "m", "y"] # Default colours for comparison

parser = argparse.ArgumentParser()
parser.add_argument("files", nargs="+")
parser.add_argument("-v", "--video", action="store_true", help="generate a video instead of an image")
parser.add_argument("-s", "--speed", default=1, type=float, help="set the speed multiplier for the video (default=1)")
parser.add_argument("-o", "--output", nargs="?", default=None, help="save the file with the filename given")
parser.add_argument("-e", "--extra_args", default=None, help="extra args to ffmpeg for video")
parser.add_argument("--static", action="store_true", help="make the video stay at the same spot")
parser.add_argument("-m", "--map", default=None, help="add a track map to the background of the video")
parser.add_argument("--minX", default=None, type=int)
parser.add_argument("--maxX", default=None, type=int)
parser.add_argument("--minZ", default=None, type=int)
parser.add_argument("--maxZ", default=None, type=int)
args = parser.parse_args()

datas = []
for file in args.files:
    datas.append(pd.read_csv(file))

boat_width = 0.6875
if args.video and not args.static:
    boat_width *= 10

fig = plt.figure()
ax = fig.add_subplot()

# Set borders
if args.video and not args.static:
    ax.set_xlim(datas[0]["x"][0] - 96, datas[0]["x"][0] + 96)
    ax.set_ylim(datas[0]["z"][0] + 54, datas[0]["z"][0] - 54)
    fig.set_size_inches(19.2, 10.8)
else:
    minX = int(min(min(data["x"]) for data in datas))
    maxX = int(max(max(data["x"]) for data in datas))
    minZ = int(min(min(data["z"]) for data in datas))
    maxZ = int(max(max(data["z"]) for data in datas))
    ax.set_xlim(minX - 64, maxX + 64)
    ax.set_ylim(maxZ + 64, minZ - 64)
    fig.set_size_inches((maxX - minX + 128) / 100, (maxZ - minZ + 128) / 100)
    if args.video:
        fig.set_size_inches((maxX - minX + 128) / (maxZ - minZ + 128) * 10.8, 10.8)
        boat_width = 10.8 / ((maxZ - minZ + 128) / 100)

ax.axes.set_aspect("equal")
fig.subplots_adjust(left=0, bottom=0, right=1, top=1)
plt.axis("off")

# Map background
if args.map is not None:
    if args.minX is None or args.maxX is None or args.minZ is None or args.maxZ is None:
        print(sys.argv[0] + ": error: --minX, --maxX, --minZ, --maxZ required when --map is used")
        exit()
    img = ax.imshow(plt.imread(args.map), extent=[args.minX, args.maxX, args.maxZ, args.minZ])

if args.video:
    lines = []
    for i, data in enumerate(datas):
        lines.append(ax.plot([], [], colours[i % len(colours)], lw=boat_width)[0])
    if args.map:
        lines.append(img)

    def run(t):
        for i, data in enumerate(datas):
            lines[i].set_data(data["x"][:t + 1], data["z"][:t + 1])
            # Recenter the camera on the first boat
            if not args.static:
                ax.set_xlim(datas[0]["x"][t] - 96, datas[0]["x"][t] + 96)
                ax.set_ylim(datas[0]["z"][t] + 54, datas[0]["z"][t] - 54)
        return *lines, 

    ani = animation.FuncAnimation(fig, run, frames=min(len(data) for data in datas), interval=int(50 / args.speed), blit=True, repeat=False)
else:
    for i, data in enumerate(datas):
        ax.plot(data["x"], data["z"], colours[i % len(colours)], lw=boat_width)

if args.output is not None:
    if args.video:
        if args.extra_args:
            ani.save(args.output, fps=int(20 * args.speed), extra_args=args.extra_args.split(" "))
        else:
            ani.save(args.output, fps=int(20 * args.speed))
    else:
        plt.savefig(args.output)
    print(f"{sys.argv[0]}: {'video' if args.video else 'image'} saved as {args.output}", end="")
else:
    plt.show()
