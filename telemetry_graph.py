import argparse
import plotly.subplots as sub
import plotly.express as px
import pandas as pd
import numpy as np

colours = ['#636efa', '#EF553B', '#00cc96', '#ab63fa', '#FFA15A', '#19d3f3', '#FF6692', '#B6E880', '#FF97FF', '#FECB52'] # Default colours for comparison
display = {"speed": 4, "slipAngle": 2}

parser = argparse.ArgumentParser()
parser.add_argument("files", nargs="+")
parser.add_argument("-e", "--extended", action="store_true", help="shows additional data (rotational speed, acceleration)")
parser.add_argument("-k", "--key_input", action="store_true", help="shows keyboard input")
parser.add_argument("-w", "--walltap", action="store_true", help="make acceleration graph more readable by removing outliers caused by walltaps")
parser.add_argument("-i", "--invert", action="store_true", help="invert the direction of rotation in the graph")
parser.add_argument("-d", "--delta", nargs="?", default=None, help="compare the times with the delta file given")
args = parser.parse_args()

df_list = []
for file in args.files:
    df_list.append(pd.read_csv(file))

tickrate = 20

def normalise_angle(angle):
    return ((angle + 180) % 360) - 180

if args.extended:
    display = {"speed": 4, "lonAcc": 1, "latAcc": 1, "slipAngle": 2, "rotSpeed": 2}
    for df in df_list:
        if "directionFacing" not in df:
            continue
        df["rotSpeed"] = (df["directionFacing"] - df["directionFacing"].shift(1)) * tickrate
        df["lonAcc"] = (df["speed"] - df["speed"].shift(1)) * tickrate
        dir_moving = df["directionFacing"] - df["slipAngle"]
        df["latAcc"] = np.sin(np.radians(dir_moving - dir_moving.shift(1)) / 2) * df["speed"].shift(1) * 2 * tickrate

if args.key_input:
    display["steering"] = 1
    display["throttle"] = 1

if args.invert:
    for df in df_list:
        for key in ["latAcc", "slipAngle", "rotSpeed", "steering"]:
            if key in df:
                df[key] = -df[key]

def calc_distance(x, z, cp):
    return x * checkpoints["xDir"][cp] + z * checkpoints["zDir"][cp]

def gen_df_interpolated(df):
    # change index of df from time to distance
    old = df.transpose()
    new_dict = {}
    delta = []
    cp = 0
    diff = 0
    for i in range(len(df)):
        if i == 0 or cp >= len(checkpoints):
            continue

        dp = calc_distance(df["x"][i], df["z"][i], cp)
        while dp > checkpoints["dist"][cp]:
            dp_last = calc_distance(df["x"][i - 1], df["z"][i - 1], cp)
            sub_tick = (checkpoints["dist"][cp] - dp_last) / (dp - dp_last)
            sub_tick_time = (i - 1 + sub_tick) / tickrate
            if cp == 0:
                start_time = sub_tick_time
            diff = sub_tick_time - start_time - checkpoints["time"][cp]
            new_dict[sub_tick_time] = (1 - sub_tick) * old[i - 1] + sub_tick * old[i]
            delta.append(diff)
            cp += 1
            if cp >= len(checkpoints):
                break
            dp = calc_distance(df["x"][i], df["z"][i], cp)

    new = pd.DataFrame.from_dict(new_dict).transpose()
    new["delta"] = delta
    return new

if args.delta is not None:
    checkpoints = pd.read_csv(args.delta)
    for i, data in enumerate(df_list):
        df_list[i] = gen_df_interpolated(data)
    display["delta"] = 1

fig = sub.make_subplots(rows=len(list(display.values())), shared_xaxes=True, vertical_spacing=0, row_heights=list(display.values()), row_titles=list(display.keys()))
for i in range(len(df_list)):
    time = [i * 0.05 for i in range(len(df_list[i]))]
    plot_list = [px.line(y=df_list[i][key], x=time)["data"][0] for key in display]
    for plot in plot_list:
        plot["line"]["color"] = colours[i % len(colours)]
    fig.add_traces(plot_list, rows=list(range(1, 1 + len(display))), cols=[1] * len(display))

if args.walltap and args.extended:
    fig.update_layout(yaxis2={"range": [-40, 20]}, yaxis3={"range": [-20, 20]})

fig.show()
